﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWSADController : MonoBehaviour
{
    public Transform target;

    public Vector3 offset;

    public float pitch = 2f;

    public float maxZoom = 15f;

    public float yawSpeed = 100f;

    public float minZoom = 5f;

    public float currentYaw = 0f;

    private float currentZoom = 10f;

    public Rigidbody player;

    private void Update()
    {
        currentZoom -= Input.GetAxis("Mouse ScrollWheel") * currentZoom;
        currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);
        if (!Input.GetKey(KeyCode.Mouse1))
        {
            currentYaw += Input.GetAxis("Mouse X") * yawSpeed * Time.deltaTime;
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = target.position - offset * currentZoom;
        transform.LookAt(target.position + Vector3.up * pitch);
        transform.RotateAround(target.position, Vector3.up, currentYaw);
    }
}
