﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombatWSAD : MonoBehaviour
{
    PlayerAnimatorWSAD playerAnimator;
    public BoxCollider axe;
    protected CharacterStats myStats;
    public Transform targetZone;
    public float radius = 0.8f;
    public float attackSpeed = 1f;
    protected float attackCooldown = 0f;
    public float attackDelay = .6f;
    public void Attack()
    {
        if (attackCooldown <= 0f)
        {
            // StartCoroutine(DoDamage(targetStats, attackDelay));
            // Zadawanie obrażeń
            playerAnimator.OnAttack();
            attackCooldown = 1f / attackSpeed;
        }
    }

    void Start()
    {
        playerAnimator = GetComponent<PlayerAnimatorWSAD>();
        
        myStats = GetComponent<CharacterStats>();
    }

    public bool combatStance =false;
    void Update()
    {
        if (Input.GetKey(KeyCode.F))
        {
            
            playerAnimator.Block();
            Debug.Log("BLOCK");

        }
        attackCooldown -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.R))
        {
            combatStance = !combatStance;
            playerAnimator.EquipWeapon();
            
        }
        if(Input.GetButtonDown("Fire1") && combatStance)
        {
            Attack();
        }
    }
}
