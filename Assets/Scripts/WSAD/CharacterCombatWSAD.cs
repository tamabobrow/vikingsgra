﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CharacterCombatWSAD : MonoBehaviour
{
    protected CharacterStats myStats;
    public float attackSpeed = 1f;
    protected float attackCooldown = 0f;
    public event System.Action OnAttack;
    public float attackDelay = .6f;
    private void Start()
    {
        myStats = GetComponent<CharacterStats>();
    }

    private void Update()
    {
        attackCooldown -= Time.deltaTime;
    }
    public virtual void Attack()
    {
        if (attackCooldown <= 0f)
        {
            // StartCoroutine(DoDamage(targetStats, attackDelay));

            if (OnAttack != null)
                OnAttack();
            attackCooldown = 1f / attackSpeed;

        }

    }
    IEnumerator DoDamage(CharacterStats stats, float delay)
    {
        yield return new WaitForSeconds(delay);

        stats.takeDamage(myStats.damage.GetValue());
    }
}
