﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : CharacterCombat
{
    public override void Attack(CharacterStats targetStats)
    {
        if (attackCooldown <= 0f)
        {
            if (myStats.currentStamina >= 5)
            {
                Debug.Log("ATAK HERO");
                base.Attack(targetStats);
                myStats.DecreaseStamina(5);
            }
            else
            {
                Debug.Log("ZA MALO staminy PASSSS");
                myStats.DecreaseStamina(myStats.maxStamina);
            }
            attackCooldown = 1f / attackSpeed;

        }


    }
}
