﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Round : MonoBehaviour
{
    #region Singleton
    public static Round instance;

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than One instance of Round Manager");
            return;
        }
        instance = this;
    }
    #endregion
    public bool TurnInProgress = false;
    public CombatMode combatMode;
    PlayerManager player;
    bool characterFight;
    List<Enemy> enemiesWaitingForRound = new List<Enemy>();
    List<bool> enemiesFinishTurn = new List<bool>();

    void Start()
    {
        combatMode = CombatMode.instance;
        player = PlayerManager.instance;
        
    }

    
    
    void RoundStart()
    {
        characterFight = false;
        foreach (Enemy element in combatMode.enemiesInCombat)
        {
            enemiesWaitingForRound.Add(element);
            enemiesFinishTurn.Add(false);
        }
        enemiesWaitingForRound.Sort(CompareBySpeed);
        if(enemiesWaitingForRound.Count == 0)
        {
            combatMode.EndCombat();

        }
    }

    public void PlayerZeroStamina()
    {
        player.platerStats.DecreaseStamina(player.platerStats.maxStamina + 1);
    }

    void TurnManage()
    {
        if(!TurnInProgress)
        {
            if (enemiesFinishTurn.Count() == 0)
            {
                RoundStart();
            }
            else
            {
                for (int i = 0; i < enemiesWaitingForRound.Count; i++)
                {
                    if (player.platerStats.speed >= enemiesWaitingForRound[i].getSpeed())
                    {
                        if (!characterFight)
                        {
                            
                            Debug.Log("Tura gracza");
                            characterFight = true;
                            TurnInProgress = true;
                            combatMode.playerTurn = true;
                            player.playerController.playerMotor.SetCombatStartPosition();
                            return;
                        }

                        if(!enemiesFinishTurn[i])
                        {
                            Debug.Log("Tura " + enemiesWaitingForRound[i].name);
                            TurnInProgress = true;
                            enemiesFinishTurn[i] = true;
                            enemiesWaitingForRound[i].startTurn();
                            return;
                        }
                        
                    }
                    else
                    {
                        if (!enemiesFinishTurn[i])
                        {
                            Debug.Log("Tura " + enemiesWaitingForRound[i].name);
                            TurnInProgress = true;
                            enemiesFinishTurn[i] = true;
                            enemiesWaitingForRound[i].startTurn();
                            return;
                        }
                    }
                }
                if (!characterFight)
                {
                    Debug.Log("Tura gracza na koniec");
                    characterFight = true;
                    TurnInProgress = true;
                    combatMode.playerTurn = true;
                    player.playerController.playerMotor.SetCombatStartPosition();
                    return;
                }
                RoundEnd();
            }
        }

    }




    void RoundEnd()
    {
        enemiesWaitingForRound.Clear();
        enemiesFinishTurn.Clear();
        Debug.Log("Koniec Rundy");
    }

    void YourTurn()
    {

    }
    void Update()
    {
        if(combatMode.combat)
        {
            TurnManage();
        }
    }

    private static int CompareBySpeed(Enemy x, Enemy y)
    {
        return x.getSpeed().CompareTo(y.getSpeed());
    }
}
