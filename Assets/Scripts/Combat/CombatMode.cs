﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatMode : MonoBehaviour
{
    #region Singleton
    public static CombatMode instance;

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than One instance of Player");
            return;
        }
        instance = this;
    }
    #endregion

    public bool combat = false;
    public List<Enemy> enemiesInCombat = new List<Enemy>();
    public bool playerTurn = false;
    Round round;

    private void Start()
    {
        round = Round.instance;
    }

    public void StartCombat()
    {
        combat = true;
        
    }
    public void EndCombat()
    {
        combat = false;
        Debug.Log("Koniec Walki");
        round.TurnInProgress = false;
        round.PlayerZeroStamina();
        
    }

    public void AddEnemyInCombat(Enemy enemy)
    {
        enemiesInCombat.Add(enemy);
    }

    public void EnemieDie(Enemy enemy)
    {
        enemiesInCombat.Remove(enemy);
        if(enemiesInCombat.Count == 0)
        {
            EndCombat();
            

        }
    }
}
