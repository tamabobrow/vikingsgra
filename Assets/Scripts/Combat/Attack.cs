﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Attack", menuName = "Attacks")]
public class Attack : ScriptableObject
{
    public float range;
    public string attackName = "New item";
    public Sprite icon = null;


    public virtual void AttackTarget()
    {
        Debug.Log("Zaatakowano " + attackName);
    }

}

public enum AttackType { Melee, Distance, Legs, Weapon, Shield, Feet }
