﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestsManager : MonoBehaviour
{
    #region Singleton
    public static QuestsManager instance;

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than One instance of QuestsManager");
            return;
        }
        instance = this;
    }
    #endregion

    private List<Quest> questsList;

    public void Add(Quest q)
    {
        questsList.Add(q);
    }

}
