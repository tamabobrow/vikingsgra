﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Quest", menuName = "Quest")]
public class Quest : ScriptableObject
{
    QuestsManager manager;
    public string nazwa;
    public string opis;
    public float exp;
    public Item[] items;

    public void startQuest()
    {
        Debug.Log("Dostałeś nowe zadanie");
        manager = QuestsManager.instance;
        manager.Add(this);
    }

    public void questDone()
    {
        // Dostajemy expa
        // dostajemy itemy
        // Dropi itemy
        Debug.Log("Zadanie zostało wykonane");
    }

}
