﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseScript : MonoBehaviour
{
    protected PlayerManager playerManager;
    protected CombatMode combatMode;
    void Start()
    {
        playerManager = PlayerManager.instance;
        combatMode = CombatMode.instance;
    }

    public virtual void Use()
    {

    }
}
