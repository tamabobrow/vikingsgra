﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : UseScript
{
    public override void Use()
    {
        if(combatMode.combat && combatMode.playerTurn)
        {
            Debug.Log("Uzyto UseScript");
            playerManager.platerStats.DecreaseStamina(playerManager.platerStats.maxStamina + 1);
        }

    }
}
