﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill", menuName = "Skill")]
public class Skill : ScriptableObject
{

    public int range;
    public int damageModifier;
    public int accurate;
    public Sprite sprite;
    public UseScript useScript;


}
