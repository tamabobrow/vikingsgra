﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSlot : MonoBehaviour
{
    public string key;
    public Skill skill;
    UseScript useScript;
    public Image icon;
    public Text text;
    public Button useButton;

    private void Start()
    {
        useScript = GetComponent<UseScript>();
    }
    public void useSkill()
    {
        Debug.Log("uzyto useSkill slot");
        useScript.Use();
    }
    private void Update()
    {
        if (Input.GetKeyDown(key))
        {
            Debug.Log("uzyto useSkill slot w przycisku");
            useSkill();
        }
    }

    public void ClearSlot()
    {
        skill = null;
        icon.enabled = false;
        text.enabled = false;
        icon.sprite = null;
        useButton.interactable = false;
    }

    public void AddSkill(Skill skill)
    {
        this.skill = skill;
        icon.enabled = true;
        text.enabled = true;
        icon.sprite = skill.sprite;
        useButton.interactable = true;

    }
}
