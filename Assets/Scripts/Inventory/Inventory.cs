﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton
    public static Inventory instance;

    public void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than One instance of Inventory");
            return;
        }
        instance = this;
    }
    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;
    public List<Item> itemList = new List<Item>();
    public int space = 20;

    public bool AddItem(Item item)
    {
        if(itemList.Count >= space)
        {
            Debug.Log("FULL EQ");
            return false;
        }
        itemList.Add(item);
        if(onItemChangedCallback !=null)
            onItemChangedCallback.Invoke();
        Debug.Log("Dodano do EQ " + item.itemName);
        return true;
    }
    public void RemoveItem(Item item)
    {

        itemList.Remove(item);
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
        
    }




}
