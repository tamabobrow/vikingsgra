﻿
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Image icon;
    public Button removeButton;
    public Button useButton;

    Item item;

    public void AddItem( Item newItem)
    {
        item = newItem;
        icon.sprite = item.icon;
        icon.enabled = true;
        removeButton.interactable = true;
        useButton.interactable = true;

    }
    public void RemoveItem()
    {
        
        Inventory.instance.RemoveItem(item);
        

    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
        useButton.interactable = false;


    }
    public void UseItem()
    {
        item.Use();
        RemoveItem();
    }
}
