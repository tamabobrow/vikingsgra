﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaterStats : CharacterStats
{
    // Start is called before the first frame update
    new void Start()
    {
        EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
        round = Round.instance;
    }

    public new void DecreaseStamina(int stamina)
    {
        currentStamina -= stamina;
        if (currentStamina <= 0)
        {

            round.TurnInProgress = false;
            round.combatMode.playerTurn = false;
        }
        currentStamina = Mathf.Clamp(currentStamina, 0, int.MaxValue);
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        if(newItem != null)
        {
            armor.AddModifier(newItem.armorModifier);
            damage.AddModifier(newItem.damageModifier);
            Debug.Log("Dodano modifier");
        }

        if(oldItem != null)
        {
            armor.RemoveModifier(oldItem.armorModifier);
            damage.RemoveModifier(oldItem.damageModifier);
        }
    }
}
