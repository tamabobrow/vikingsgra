﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : CharacterStats
{
    CombatMode combatMode;
    Enemy enemy;
    
        public new void Start()
    {
        combatMode = CombatMode.instance;
        enemy = GetComponent<Enemy>();
        round = Round.instance;


    }
    public override void Die()
    {
        base.Die();
        combatMode.EnemieDie(enemy);
        Destroy(gameObject);
    }
}
