﻿using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth { get; private set; }
    public Stat damage;
    public Stat armor;
    public int maxStamina= 11;
    public int currentStamina = 0;
    public int speed;
    protected Round round;
    public void Start()
    {
        round = Round.instance;
    }
    public void Awake()
    {
        currentHealth = maxHealth;
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            takeDamage(2);
        }
    }

    public void takeDamage(int damage)
    {
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);

        currentHealth -= damage;
        Debug.Log("zytko - " + damage + " = " + currentHealth);

        if(currentHealth <= 0)
        {
            Die();
        }
    }

    public void renewStamina()
    {
        currentStamina = maxStamina;
    }

    public void DecreaseStamina(int stamina)
    {
        currentStamina -= stamina;
        if(currentStamina<= 0)
        {
           
            round.TurnInProgress = false;
        }
        currentStamina = Mathf.Clamp(currentStamina, 0, int.MaxValue);
    }

    public virtual void Die()
    {
        Debug.Log(transform.name + " umarł");
    }


}
