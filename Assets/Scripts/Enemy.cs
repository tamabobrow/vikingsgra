﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Interactable
{
    public bool yourTurn = false;
    PlayerManager playerManager;
    EnemyController enemyController;
    CharacterStats mySats;
    public bool inCombatRange = false;
    Round round;
    CharacterCombat playerCombat;

    public void Start()
    {
        playerManager = PlayerManager.instance;
        round = Round.instance;
        mySats = GetComponent<CharacterStats>();
        enemyController = GetComponent<EnemyController>();
        playerCombat = playerManager.player.GetComponent<CharacterCombat>();
    }


    public override void Interact()
    {
        Debug.Log("Inerakcja z enemy");
        
       CharacterCombat playerCombat = playerManager.player.GetComponent<CharacterCombat>();
        if(playerCombat != null)
        {
         playerCombat.Attack(mySats);    
        }

    }
    public int getSpeed()
    {
        return mySats.speed;
    }
    public void startTurn()
    {
        yourTurn = true;
        mySats.renewStamina();
        enemyController.setDistance();
    }
}
