﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public float lookRadius = 10f;
    bool inCombat = false;
    CombatMode combatMode;
    EnemyCombat enemyCombat;
    Round round;
    Enemy enemy;
    Transform target;
    NavMeshAgent agent;
    CharacterStats stats;
    Vector3 roundStartPosition;
    public bool inCombatRange;

    void Start()
    {
        stats = GetComponent<CharacterStats>();
        enemyCombat = GetComponent<EnemyCombat>();
        target = PlayerManager.instance.player.transform;
        combatMode = CombatMode.instance;
        round = Round.instance;
        enemy = GetComponent<Enemy>();
        agent = GetComponent<NavMeshAgent>();
       
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if(distance <= lookRadius)
        {
            if(!inCombat)
            {
                Debug.Log("WALKA!");
                combatMode.combat = true;
                combatMode.AddEnemyInCombat(enemy);
                inCombat = true;
            }
            if(stats.currentStamina>0)
            {
                combatMovment();
               
            }
            else
            {
                agent.ResetPath();
            }

            
        }
    }
    void combatMovment()
    {

        float distance = Vector3.Distance(roundStartPosition, transform.position);
        
        if(distance >= 5)
        {
            roundStartPosition = transform.position;
            distance = 0;
            stats.DecreaseStamina(1);
        }
        agent.SetDestination(target.position);
        if(Vector3.Distance(target.position, transform.position)<=4f)
        {
            enemyCombat.Attack(PlayerManager.instance.platerStats);
            agent.ResetPath();
        }
        else
        {
            inCombatRange = false;
        }
        FaceTarget();

    }
    public void setDistance()
    {
        roundStartPosition = transform.position;
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookAround = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookAround, Time.deltaTime * 5f); 

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

}
