﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestItem : ItemPickup
{
    public Quest quest;
    public override void Interact()
    {
        base.Interact();
        quest.questDone();
    }
}
