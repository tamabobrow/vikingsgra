﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterUI : MonoBehaviour
{
    PlaterStats platerStats;
    PlayerManager playerManager;
    EquipmentManager equipmentManager;
    public GameObject characterUI;
    public Text zytkoMaxText;
    public Text speedText;
    public Text armorText;
    public Text damageText;
    public Text staminaMaxText;
    // Start is called before the first frame update
    void Start()
    {
        playerManager = PlayerManager.instance;
        platerStats = playerManager.platerStats;
        equipmentManager = EquipmentManager.instance;
        zytkoMaxText.text = "Max zytko = " + platerStats.maxHealth;
        speedText.text = "Speed = " + platerStats.speed;
        armorText.text = "Armor = " + platerStats.armor.GetValue();
        damageText.text = "Damage = " + platerStats.damage.GetValue();
        staminaMaxText.text = "Max Stamina = " + platerStats.maxStamina;


    }
    private void Update()
    {
        if (Input.GetButtonDown("Character"))
        {
            characterUI.SetActive(!characterUI.activeSelf);
            StatsUpdate();
        }
    }

    // Update is called once per frame
    void StatsUpdate()
    {
        zytkoMaxText.text = "Max zytko = " + platerStats.maxHealth;
        speedText.text = "Speed = " + platerStats.speed;
        armorText.text = "Armor = " + platerStats.armor.GetValue();
        damageText.text = "Damage = " + platerStats.damage.GetValue();
        staminaMaxText.text = "Max Stamina = " + platerStats.maxStamina;
    }
}
