﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterEquipmentSlot : MonoBehaviour
{
    public Image icon;
    public int slot;
    EquipmentManager equipmentManager;

    // Start is called before the first frame update
    void Start()
    {
        equipmentManager = EquipmentManager.instance;
        equipmentManager.onEquipmentChanged += ChangeUI;
    }

  

    void ChangeUI(Equipment newItem,Equipment oldItem)
    {
        if (equipmentManager.currentEquipment[slot] != null)
        {
            icon.enabled = true;
            icon.sprite = equipmentManager.currentEquipment[slot].icon;
        }
        else
        {
            icon.enabled = false;
        }
    }
}
