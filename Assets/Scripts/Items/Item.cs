﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item ")]
public class Item : ScriptableObject
{
    
    public string itemName = "New item";
    public int cost = 0;
    public Sprite icon = null;

    public virtual void Use()
    {
        Debug.Log("Uzyto " + itemName);
    }

}
