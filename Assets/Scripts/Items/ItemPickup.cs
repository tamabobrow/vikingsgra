﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : Interactable
{
    public PlayerAnimatorWSAD animatorWSAD;
    public Item item;
    public override void Interact()
    {
        animatorWSAD.PickUp();
        PickUp();
    }
    void PickUp()
    {
        
        if(Inventory.instance.AddItem(item))
        {
            Debug.Log("Podniesiono " + item.itemName);
            Destroy(gameObject);
        }
        
    }

}
