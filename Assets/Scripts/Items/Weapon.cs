﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Weapon", menuName = "Inventory/Weapon")]

public class Weapon : Equipment
{
   public WeaponType weaponType;
}
public enum WeaponType { Sword, Bow, Dagger, Axe, Staff }