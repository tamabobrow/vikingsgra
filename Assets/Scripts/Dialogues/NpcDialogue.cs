﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NpcDialogue : MonoBehaviour
{
    private Dialogue dia;
    public TextMeshProUGUI dialogueText;
    private Canvas dialogue_window;
    public GameObject dialoguePanel;
    public GameObject optionsPanel;
    private GameObject node_text;
    public Button option_1;
    public Button option_2;
    public Button option_3;
    public Button option_4;
    private GameObject exit;
    private DialogueNode currentNode;

    private int selected_option = -2;

    public string DialogueDataFilePath;

    public GameObject DialogueWindowPrefab;

    private void Start()
    {
        dia = Dialogue.LoadDialogue("Assets/Dialogues/" +DialogueDataFilePath);
        currentNode = dia.Nodes[0];
        Debug.Log(currentNode.Options.Count);
        option_1.onClick.AddListener(Option1Click);
        option_2.onClick.AddListener(Option2Click);
       // option_3.onClick.AddListener(Option3Click);
       // option_4.onClick.AddListener(Option4Click);
        dialogueText.text = currentNode.Text;
        option_1.GetComponentInChildren<Text>().text = currentNode.Options[0].Text;
        option_2.GetComponentInChildren<Text>().text = currentNode.Options[1].Text;
        //option_3.name = currentNode.Options[2].Text;
        //option_4.name = currentNode.Options[3].Text;
    }
    private void Option1Click()
    {
        currentNode = dia.Nodes[currentNode.Options[0].DestinationNodeID];
        ChangeDialogue();
    }
    private void Option2Click()
    {
        currentNode = dia.Nodes[currentNode.Options[1].DestinationNodeID];
        ChangeDialogue();
    }
    private void Option3Click()
    {
        currentNode = dia.Nodes[currentNode.Options[2].DestinationNodeID];
    }
    private void Option4Click()
    {
        currentNode = dia.Nodes[currentNode.Options[3].DestinationNodeID];
    }
    private void ChangeDialogue()
    {
        dialogueText.text = currentNode.Text;
        option_1.GetComponentInChildren<Text>().text = currentNode.Options[0].Text;
        option_2.GetComponentInChildren<Text>().text = currentNode.Options[1].Text;
        option_3.GetComponentInChildren<Text>().text = currentNode.Options[2].Text;
        option_4.GetComponentInChildren<Text>().text = currentNode.Options[3].Text;
    }
}
