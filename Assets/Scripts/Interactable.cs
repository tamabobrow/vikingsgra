﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;

    bool isFocus = false;
    bool hasInteract = false;
    Transform player;

    public virtual void Interact()
    {
        Debug.Log("Interakcja");
    }

    private void Update()
    {
       if(isFocus && !hasInteract)
        {
            float distance = Vector3.Distance(player.position, transform.position);
            if (distance <= radius)
            {
                Interact();
                hasInteract = true;
            }
        }
        
    }

    public void OnFocus(Transform playerTransform)
    {
        isFocus = true;
        player = playerTransform;
        hasInteract = false;
        Debug.Log("Focus " + transform.name);
    }
    public void OnDeFocus()
    {
        isFocus = false;
        player = null;
        hasInteract = false;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
