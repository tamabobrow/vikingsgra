﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Singleton
    public static PlayerManager instance;

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than One instance of Player");
            return;
        }
        instance = this;
    }
    #endregion
    public PlaterStats platerStats;
    public Skill currentSkillUse;
    public GameObject player;
    public PlayerController playerController;  
        public void Start()
    {
        playerController = player.GetComponent<PlayerController>();
    }

    public void SetCurrentSkill(Skill skill)
    {
        currentSkillUse = skill;
    }
}
