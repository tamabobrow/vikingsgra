﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    public PlayerAnimatorWSAD animator;
    private CharacterController playerRigid;
    public float speed = 0.1f;
    public float jump = 2;

    public float gravity = -9.81f;
    Vector3 velocity;
    float mouseX;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask layerMask;

    bool isGrounded;

    public float yawSpeed = 100f;
    void Start()
    {
        playerRigid = GetComponent<CharacterController>();
        animator = GetComponent<PlayerAnimatorWSAD>();
        speed *= Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, layerMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jump * -2f * gravity);
            animator.Jump();
        }
        if (!Input.GetKey(KeyCode.Mouse1))
        {
            mouseX = Input.GetAxis("Mouse X") * yawSpeed * Time.deltaTime;
            transform.Rotate(Vector3.up * mouseX);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }


        Vector3 move = transform.right * x + transform.forward * z;

        velocity.y += 0.5f * gravity * Time.deltaTime;

        playerRigid.Move(velocity * Time.deltaTime);
        
        if(isGrounded)
        {
            playerRigid.Move(move * speed);
            velocity.x = 0;
            velocity.z = 0;
        }
        else
        {
            velocity.x += move.x * 2*speed;
            velocity.z += move.z * 2*speed;
        }
        
    }
}
