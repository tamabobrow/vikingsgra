﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerAnimatorWSAD : MonoBehaviour
{
    public AnimationClip[] defultAttackAnimSet;
    protected AnimationClip[] currentAttackAnimSet;
    public AnimationClip replaceableClip;
    private bool combat = false;
    const float locomotivAnimationSmoothTime = .1f;
    protected Animator animator;
    CharacterController playerRigid;
    protected AnimatorOverrideController overrideController;
    protected virtual void Start()
    {
        playerRigid = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        //combatMode = CombatMode.instance;
        overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = overrideController;

        currentAttackAnimSet = defultAttackAnimSet;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        float speedPrecent = playerRigid.velocity.magnitude;
        animator.SetFloat("speedPercent", speedPrecent, locomotivAnimationSmoothTime, Time.deltaTime);

       // animator.SetBool("inCombat", combatMode.combat);
    }

    public virtual void OnAttack()
    {
        animator.SetTrigger("attack");
        int attackIndex = Random.Range(0, currentAttackAnimSet.Length);
        overrideController[replaceableClip.name] = currentAttackAnimSet[attackIndex];
    }

    public virtual void Jump()
    {
        animator.SetTrigger("jump");
    }
    public virtual void Block()
    {
        animator.SetTrigger("block");
    }
    public virtual void EquipWeapon()
    {
        combat = !combat;
        animator.SetTrigger("equipWeapon");
        animator.SetBool("inCombat", combat);

    }

    public void PickUp()
    {
        animator.SetTrigger("pickUp");
    }

}
