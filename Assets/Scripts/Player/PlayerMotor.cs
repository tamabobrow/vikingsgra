﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerMotor : MonoBehaviour
{
    PlaterStats platerStats;
    Transform target;
    Vector3 startTurnPostion;
    public NavMeshAgent agent;
    CombatMode combatMode;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        platerStats = GetComponent<PlaterStats>();
        combatMode = CombatMode.instance;
    }

    // Update is called once per frame

    private void Update()
    {
        if(target != null)
        {
            MoveToPoint(target.position);
            FaceTarget();

        }

    }
        

    public void SetCombatStartPosition()
    {
        startTurnPostion = this.transform.position;
        agent.ResetPath();
        platerStats.renewStamina();
        Debug.Log("O BOGOWIE WALKA!");
    }
    public void MoveToPoint(Vector3 point)
    {
        if (!combatMode.combat)
        {
            agent.SetDestination(point);
            Debug.Log("Poruszamy sie bez combatu");
            
        }
        else if(combatMode.playerTurn)
        {
            float distance = Vector3.Distance(startTurnPostion, transform.position);

            if (distance >= 5)
            {
                startTurnPostion = transform.position;
                distance = 0;
                platerStats.DecreaseStamina(1);
            }
            agent.SetDestination(point);
            
        }
        else
        {
            agent.ResetPath();
        }
            
    }

    public void FollowTarget(Interactable newTarget)
    {
        agent.stoppingDistance = newTarget.radius * .92f;
        agent.updateRotation = false;
        target = newTarget.transform;
    }

    public void StopFollowingTarget()
    {
        agent.stoppingDistance = 0f;
        agent.updateRotation = true;
        target = null;
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime* 5f);
    }
}
