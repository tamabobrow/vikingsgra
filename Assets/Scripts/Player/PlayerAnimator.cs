﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerAnimator : MonoBehaviour
{
    protected CharacterCombat characterCombat;
    public AnimationClip[] defultAttackAnimSet;
    protected AnimationClip[] currentAttackAnimSet;
    public AnimationClip replaceableClip;

    const float locomotivAnimationSmoothTime = .1f;
    protected Animator animator;
    CombatMode combatMode;
    NavMeshAgent navMesh;
    protected AnimatorOverrideController overrideController;
    protected virtual void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        combatMode = CombatMode.instance;
        characterCombat = GetComponent<CharacterCombat>();
        overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = overrideController;

        currentAttackAnimSet = defultAttackAnimSet;
        characterCombat.OnAttack += OnAttack;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        float speedPrecent = navMesh.velocity.magnitude / navMesh.speed;
        animator.SetFloat("speedPercent", speedPrecent, locomotivAnimationSmoothTime, Time.deltaTime);
        animator.SetBool("inCombat", combatMode.combat); 
    }

    protected virtual void OnAttack()
    {
        animator.SetTrigger("attack");
        int attackIndex = Random.Range(0, currentAttackAnimSet.Length);
        overrideController[replaceableClip.name] = currentAttackAnimSet[attackIndex];
    }
}
