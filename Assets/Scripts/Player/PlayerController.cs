﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    Camera cam;
    public LayerMask movmentMask;
    public LayerMask interactionMask;
    CombatMode combatMode;
    Interactable focus;
    public PlayerMotor playerMotor;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        playerMotor = GetComponent<PlayerMotor>();
        combatMode = CombatMode.instance;


    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RemoveFocus();
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, 100, movmentMask))
            {
                playerMotor.MoveToPoint(hit.point);
            }
            if (Physics.Raycast(ray, out hit, 100))
            {
                Interactable interactable = hit.collider.GetComponent<Interactable>();
                if(interactable!= null)
                {
                    SetFocus(interactable);
                  
                }

            }
        }
    }




    void SetFocus(Interactable newFocus)
    {
        if(focus != newFocus)
        {
            if(focus !=null)
            {
                focus.OnDeFocus();
            }

            focus = newFocus;
            
            playerMotor.FollowTarget(focus); 
        }

        newFocus.OnFocus(transform);

    }
    void RemoveFocus()
    {
         if(focus != null)
            focus.OnDeFocus();
        focus = null;
        playerMotor.StopFollowingTarget();
    }

}
