﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTrigger : MonoBehaviour
{
     Animator playerAnimator;
    public float delay = 2f;
    private void Start()
    {
        playerAnimator = GetComponentInParent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("kolizja z " + other.name);
        playerAnimator.SetTrigger("colision");
        Debug.Log("Animator OFF");
        StartCoroutine(ResumeAnimation(delay));
    }
    IEnumerator ResumeAnimation(float delay)
    {
        yield return new WaitForSeconds(delay);
        
    }
}
